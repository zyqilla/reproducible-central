[io.github.sebastian-toepfer.json.rpc:json-printable-maven-plugin](https://central.sonatype.com/artifact/io.github.sebastian-toepfer.json.rpc/json-printable-maven-plugin/versions) RB check
=======

[![Reproducible Builds](https://reproducible-builds.org/images/logos/rb.svg) an independently-verifiable path from source to binary code](https://reproducible-builds.org/)

## Project: [io.github.sebastian-toepfer.json.rpc:json-printable-maven-plugin](https://central.sonatype.com/artifact/io.github.sebastian-toepfer.json.rpc/json-printable-maven-plugin/versions)

Source code: [https://github.com/sebastian-toepfer/json-printable-maven-plugin.git](https://github.com/sebastian-toepfer/json-printable-maven-plugin.git)

rebuilding **2 releases** of io.github.sebastian-toepfer.json.rpc:json-printable-maven-plugin:
- **2** releases were found successfully **fully reproducible** (100% reproducible artifacts :heavy_check_mark:),
- 0 had issues (some unreproducible artifacts :warning:, see eventual :mag: diffoscope and/or :memo: issue tracker links):

| version | [build spec](/BUILDSPEC.md) | [result](https://reproducible-builds.org/docs/jvm/): reproducible? | size |
| -- | --------- | ------ | -- |
| [0.1.3](https://central.sonatype.com/artifact/io.github.sebastian-toepfer.json.rpc/json-printable-maven-plugin/0.1.3/pom) | [mvn jdk17](json-printable-maven-plugin-0.1.3.buildspec) | [result](json-printable-maven-plugin-0.1.3.buildinfo): [3 :heavy_check_mark: ](json-printable-maven-plugin-0.1.3.buildcompare) | 102K |
| [0.1.2](https://central.sonatype.com/artifact/io.github.sebastian-toepfer.json.rpc/json-printable-maven-plugin/0.1.2/pom) | [mvn jdk17](json-printable-maven-plugin-0.1.2.buildspec) | [result](json-printable-maven-plugin-0.1.2.buildinfo): [3 :heavy_check_mark: ](json-printable-maven-plugin-0.1.2.buildcompare) | 102K |

<i>(size is calculated without javadoc, that has been excluded from reproducibility checks)</i>
