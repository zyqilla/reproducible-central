[org.sentrysoftware:printf4j](https://central.sonatype.com/artifact/org.sentrysoftware/printf4j/versions) RB check
=======

[![Reproducible Builds](https://reproducible-builds.org/images/logos/rb.svg) an independently-verifiable path from source to binary code](https://reproducible-builds.org/)

## Project: [org.sentrysoftware:printf4j](https://central.sonatype.com/artifact/org.sentrysoftware/printf4j/versions)

Source code: [https://github.com/sentrysoftware/printf4j.git](https://github.com/sentrysoftware/printf4j.git)

rebuilding **1 releases** of org.sentrysoftware:printf4j:
- **1** releases were found successfully **fully reproducible** (100% reproducible artifacts :heavy_check_mark:),
- 0 had issues (some unreproducible artifacts :warning:, see eventual :mag: diffoscope and/or :memo: issue tracker links):

| version | [build spec](/BUILDSPEC.md) | [result](https://reproducible-builds.org/docs/jvm/): reproducible? | size |
| -- | --------- | ------ | -- |
| [0.9.05](https://central.sonatype.com/artifact/org.sentrysoftware/printf4j/0.9.05/pom) | [mvn jdk17](printf4j-0.9.05.buildspec) | [result](printf4j-0.9.05.buildinfo): [3 :heavy_check_mark: ](printf4j-0.9.05.buildcompare) | 26K |

<i>(size is calculated without javadoc, that has been excluded from reproducibility checks)</i>
