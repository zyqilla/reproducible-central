[org.sentrysoftware:xflat](https://central.sonatype.com/artifact/org.sentrysoftware/xflat/versions) RB check
=======

[![Reproducible Builds](https://reproducible-builds.org/images/logos/rb.svg) an independently-verifiable path from source to binary code](https://reproducible-builds.org/)

## Project: [org.sentrysoftware:xflat](https://central.sonatype.com/artifact/org.sentrysoftware/xflat/versions)

Source code: [https://github.com/sentrysoftware/xflat.git](https://github.com/sentrysoftware/xflat.git)

rebuilding **1 releases** of org.sentrysoftware:xflat:
- **1** releases were found successfully **fully reproducible** (100% reproducible artifacts :heavy_check_mark:),
- 0 had issues (some unreproducible artifacts :warning:, see eventual :mag: diffoscope and/or :memo: issue tracker links):

| version | [build spec](/BUILDSPEC.md) | [result](https://reproducible-builds.org/docs/jvm/): reproducible? | size |
| -- | --------- | ------ | -- |
| [1.0.00](https://central.sonatype.com/artifact/org.sentrysoftware/xflat/1.0.00/pom) | [mvn jdk17](xflat-1.0.00.buildspec) | [result](xflat-1.0.00.buildinfo): [3 :heavy_check_mark: ](xflat-1.0.00.buildcompare) | 56K |

<i>(size is calculated without javadoc, that has been excluded from reproducibility checks)</i>
