[org.sentrysoftware:winrm](https://central.sonatype.com/artifact/org.sentrysoftware/winrm/versions) RB check
=======

[![Reproducible Builds](https://reproducible-builds.org/images/logos/rb.svg) an independently-verifiable path from source to binary code](https://reproducible-builds.org/)

## Project: [org.sentrysoftware:winrm](https://central.sonatype.com/artifact/org.sentrysoftware/winrm/versions)

Source code: [https://github.com/sentrysoftware/winrm.git](https://github.com/sentrysoftware/winrm.git)

rebuilding **1 releases** of org.sentrysoftware:winrm:
- **1** releases were found successfully **fully reproducible** (100% reproducible artifacts :heavy_check_mark:),
- 0 had issues (some unreproducible artifacts :warning:, see eventual :mag: diffoscope and/or :memo: issue tracker links):

| version | [build spec](/BUILDSPEC.md) | [result](https://reproducible-builds.org/docs/jvm/): reproducible? | size |
| -- | --------- | ------ | -- |
| [1.0.00](https://central.sonatype.com/artifact/org.sentrysoftware/winrm/1.0.00/pom) | [mvn jdk17](winrm-1.0.00.buildspec) | [result](winrm-1.0.00.buildinfo): [3 :heavy_check_mark: ](winrm-1.0.00.buildcompare) | 434K |

<i>(size is calculated without javadoc, that has been excluded from reproducibility checks)</i>
