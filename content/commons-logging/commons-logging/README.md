[commons-logging:commons-logging](https://central.sonatype.com/artifact/commons-logging/commons-logging/versions) RB check
=======

[![Reproducible Builds](https://reproducible-builds.org/images/logos/rb.svg) an independently-verifiable path from source to binary code](https://reproducible-builds.org/)

## Project: [commons-logging:commons-logging](https://central.sonatype.com/artifact/commons-logging/commons-logging/versions)

Source code: [https://github.com/apache/commons-logging](https://github.com/apache/commons-logging)

rebuilding **1 releases** of commons-logging:commons-logging:
- **1** releases were found successfully **fully reproducible** (100% reproducible artifacts :heavy_check_mark:),
- 0 had issues (some unreproducible artifacts :warning:, see eventual :mag: diffoscope and/or :memo: issue tracker links):

| version | [build spec](/BUILDSPEC.md) | [result](https://reproducible-builds.org/docs/jvm/): reproducible? | size |
| -- | --------- | ------ | -- |
| [1.3.0](https://central.sonatype.com/artifact/commons-logging/commons-logging/1.3.0/pom) | [mvn jdk21](commons-logging-1.3.0.buildspec) | [result](commons-logging-1.3.0.buildinfo): [7 :heavy_check_mark: ](commons-logging-1.3.0.buildcompare) | 632K |

<i>(size is calculated without javadoc, that has been excluded from reproducibility checks)</i>
